from morrisonsutils import basic


logger = basic.MorLogging.get_logger('INFO', 'CLOUD', app_name='trevor_cleanup', use_print=True, print_debug=True)

# clean up BQ temp tables
gbqClient = basic.GoogleSetup.get_bigquery_client()
myDataset = gbqClient.dataset('ONLINE_INSIGHT_TREVOR', 'morrisonsinsight')

tempTables = list(filter(lambda x: x[1].startswith('TEMP_'),
                         map(lambda x: (x, x.table_id), gbqClient.list_tables(myDataset))))
if len(tempTables) > 0:
    logger.debug('Removing {} temporary tables'.format(str(len(tempTables))))

    for table_ref in tempTables:
        logger.debug('Deleting temp table {}'.format(table_ref[1]))
        try:
            gbqClient.delete_table(table_ref[0].reference)
        except Exception as e:
            logger.error('Failed to delete table {}'.format(table_ref[1]), exc_info=True)
        
    logger.info('{} temp Tables cleaned, now cleaning cloud storage'.format(str(len(tempTables))))

# clean up Hadoop storage
storageClient = basic.GoogleSetup.get_storage_client()

myBucket = storageClient.get_bucket('morrisonsinsight-trevor-temp')
hadoopObjects = list(myBucket.list_blobs(prefix='hadoop/tmp'))
logger.debug('Removing {} hadoop objects'.format(str(len(hadoopObjects))))
for obj in hadoopObjects:
    print('Deleting {}'.format(obj))
    obj.delete()

# clean up dataproc init actions
dataprocObjects = list(myBucket.list_blobs(prefix='google-cloud-dataproc-metainfo/'))
logger.debug('Removing {} dataproc initialization objects'.format(str(len(dataprocObjects))))
for obj in dataprocObjects:
    print('Deleting {}'.format(obj))
    obj.delete()

logger.info('Storage objects cleaned')
